name := "untitled1"

version := "1.0"

scalaVersion := "2.11.9"

mainClass in Compile := Some("fnord.WebApp")

libraryDependencies ++= Seq(
  "ch.qos.logback" % "logback-classic" % "1.2.3" % "runtime",
  "org.eclipse.jetty" % "jetty-webapp" % "9.2.19.v20160908",
  "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
  "de.ruedigermoeller" % "fst" % "2.54"
  )

