function loadPoetry(event) {
    $("#loader").toggle();
    $(".wybuch").hide();
    $.get("1", function(data) {
	$("#cont").html(data);
	$("#loader").toggle();
	$(".wybuch").show();
    });
}

function loadPoetry2(event) {
    $("#loader").toggle();
    $(".wybuch").hide();
    $.get("oda", function(data) {
	$("#cont").html(data);
	$("#loader").toggle();
	$(".wybuch").show();
    });
}

function loadPoetry3(event) {
    $("#loader").toggle();
    $(".wybuch").hide();
    $.get("pt", function(data) {
	$("#cont").html(data);
	$("#loader").toggle();
	$(".wybuch").show();
    });
}

function loadCustom(event, text) {
    $("#loader").toggle();
    $(".wybuch").hide();
    $.post("custom", $("#customInput").val(), function(data) {
	$("#cont").html(data);
	$("#loader").toggle();
	$(".wybuch").show();
    });    
}

$(document).ready(function(){
    // alert("foo")
    $("#loader").hide();
    $(".wybuch").hide();
});
 
