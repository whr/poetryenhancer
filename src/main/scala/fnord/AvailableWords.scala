package fnord

import java.io.InputStream

import scala.io.Source

/**
  * Created by whr on 20.10.2017.
  */
object AvailableWords {
  val stream : InputStream = getClass.getResourceAsStream("/available_words.txt")
  val words = scala.io.Source.fromInputStream( stream ).getLines.toSeq

  def wordsWithLength(l: Int) = words.filter(_.length == l)

  def wordsAboutLength(l: Int) = words.filter(w => Math.abs(w.length - l) == 2)
}
