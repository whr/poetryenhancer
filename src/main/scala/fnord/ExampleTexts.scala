package fnord

object ExampleTexts {

  val odaDoMlodosci =
    """
      |bez serc, bez ducha, — to szkieletów ludy!
      |młodości! dodaj mi skrzydła!
      |niech nad martwym wzlecę światem
      |w rajską dziedzinę ułudy:
      |kędy zapał tworzy cudy,
      |nowości potrząsa kwiatem
      |i obleka w nadziei złote malowidła!
    """.stripMargin

  val lokomotywa =
    """
      |stoi na stacji lokomotywa,
      |ciężka, ogromna i pot z niej spływa:
      |tłusta oliwa.
      |stoi i sapie, dyszy i dmucha,
      |żar z rozgrzanego jej brzucha bucha:
      |buch - jak gorąco!
      |uch - jak gorąco!
      |puff - jak gorąco!
      |uff - jak gorąco!
      |już ledwo sapie, już ledwo zipie,
      |a jeszcze palacz węgiel w nią sypie.
    """.stripMargin

  val paniTwardowska =
    """
      |BALLADA
      |Jedzą, piją, lulki palą,
      |Tańce, hulanka, swawola;
      |Ledwie karczmy nie rozwalą,
      |Cha cha, chi chi, hejza, hola!
      |
      |Twardowski siadł w końcu stoła.
      |Podparł się w boki jak basza;
      |"Hulaj dusza! hulaj!" - woła,
      |Śmieszy, tumani, przestrasza.
      |
      |Żołnierzowi, co grał zucha,
      |Wszystkich łaje i potrąca,
      |Świsnął szablą koło ucha,
      |Już z żołnierza masz zająca.
      |
      |Na patrona z trybunału,
      |Co milczkiem wypróżniał rondel,
      |Zadzwonił kieską pomału,
      |Z patrona robi się kondel.
      |
      |Szewcu w nos wyciął trzy szczutki,
      |Do łba przymknął trzy rureczki,
      |Cmoknął, cmok, i gdańskiej wódki
      |Wytoczył ze łba pół beczki.
    """.stripMargin
}
