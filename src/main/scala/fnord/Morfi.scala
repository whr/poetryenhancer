package fnord
import scala.collection.JavaConversions._
import pl.sgjp.morfeusz.{Morfeusz, MorfeuszUsage, MorphInterpretation}

object Morfi {
  def getTag(interpretation: MorphInterpretation) = {
    this.synchronized {
      interpretation.getTag(morfi)
    }
  }

  private val morfi = Morfeusz.createInstance("sgjp")

  def analyseAsList(word: String) = {
    this.synchronized {
      morfi.analyseAsList(word)
    }
  }

}
