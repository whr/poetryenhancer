package fnord
import sys.process._

class TextConverter {
  val replFinder = new WordReplacementFinder

  def convertText(text: String): String = {
    val wiersz = text

    val textres = wiersz.split("\n").map { wers =>
      convertLine(wers).capitalize
    }.mkString("\n")
  //  sayText(textres)
    textres
  }

  def convertTextNoAudio(text: String): String = {
    val wiersz = text

    val textres = wiersz.split("\n").map { wers =>
      convertLine(wers).capitalize
    }.mkString("\n")

    textres
  }

  private def sayText(text: String) = {
    s"say --rate=80 ${text.replaceAll("\n", ". ")}".run
  }

  private def convertLine(str: String) = {
    str.replaceAll("[^a-zA-ZęóąśżźćńłĘÓĄŁŻ]", " ").split(" ")
      .map(_.trim())
      .map(word => replFinder.findWordReplacement(word).getOrElse(word)).mkString(" ")
  }

}
