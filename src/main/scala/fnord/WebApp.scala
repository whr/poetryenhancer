package fnord

import org.eclipse.jetty.server.handler.{AbstractHandler, ContextHandler, HandlerList, ResourceHandler}
import org.eclipse.jetty.server.{Request, Server}
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import org.eclipse.jetty.util.resource.Resource

object WebApp extends App {
  val textConverter = new TextConverter

  // preload
  println("preload...")
  println(textConverter.convertTextNoAudio("wielomian"))

  class Handler extends AbstractHandler {
    override def handle(target: String,
                        req: Request,
                        httpReq: HttpServletRequest,
                        httpRes: HttpServletResponse) = {

      val text = target match {
        case "/1" => response(req, httpRes, textConverter.convertText(ExampleTexts.lokomotywa))
        case "/oda" => response(req, httpRes, textConverter.convertText(ExampleTexts.odaDoMlodosci))
        case "/pt" => response(req, httpRes, textConverter.convertText(ExampleTexts.paniTwardowska))
        case "/custom" => {
          response(req, httpRes, textConverter.convertText(readParams(req)))
        }
        case "/" =>
        case _ =>
      }

    }
  }

  def readParams(req: Request): String = {
    val sb = new StringBuilder
    var line: String = null
    val reader = req.getReader

    while({line = reader.readLine();  line!= null}) {
      sb.append(line.trim() + "\n")
    }
    sb.toString()
  }

  def response(req: Request, httpRes: HttpServletResponse, text: Any): Unit = {
    val html = s"<pre> $text </pre>"

    httpRes.setContentType("text/html")
    httpRes.setCharacterEncoding("UTF-8")

    httpRes.setStatus(HttpServletResponse.SC_OK)
    httpRes.getWriter().println(html.toString)
    req.setHandled(true)
  }

  val resourceHandler = new ResourceHandler
  println(s"PATH: ${Resource.newClassPathResource("web-static")}")
  resourceHandler.setBaseResource(Resource.newClassPathResource("web-static"))

  val handlers = new HandlerList
  handlers.addHandler(new Handler)
  handlers.addHandler(resourceHandler)

  val server = new Server(8081)
  server.setHandler(handlers)

  server.start

}