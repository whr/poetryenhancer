package fnord

import pl.sgjp.morfeusz.Morfeusz

/**
  * Created by whr on 20.10.2017.
  */


case class WordInfo(word: String, pos: String, cardinality: String, gender: Option[String])

class WordAnalyser {
  def analyseWord(word:String): Option[WordInfo] = {
   // println(s"checking word ${word}")
    //val morfi = Morfi.morfi

    val analyseResult = Morfi.analyseAsList(word)


    if(analyseResult.size() > 0) {
      val tags = Morfi.getTag(analyseResult.get(0)) //.getTag(morfi)
      //println(s"word: ${word} tags : ${tags}")
      val splitted = tags.split(":")
      if (splitted.size > 2) {
        val pos = splitted(0)
        val cardinality = splitted(1)

        val genderOption = splitted.toSeq.filter { tag => Set("f", "m", "n").contains(tag.replaceAll("[^a-z]", "")) }.headOption

        Some(WordInfo(word, pos, cardinality, genderOption))
      } else {
        None
      }

    } else {
      None
    }
  }
}


object WordAnalyser extends App {
  val foo = new WordAnalyser

  println(foo.analyseWord("samochody"))
  println(foo.analyseWord("turgotach"))
}
