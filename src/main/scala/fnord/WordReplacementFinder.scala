package fnord
import pl.sgjp.morfeusz.{Morfeusz, MorphInterpretation}

import scala.util.Random

class WordReplacementFinder {
  def findWordReplacement(word: String): Option[String] = {
    val wordAnalyser = new WordAnalyser
    val analyseResult = wordAnalyser.analyseWord(word)

    analyseResult.map { result =>
      val potentialWords = AvailableWords.wordsAboutLength(word.length + 1).map(_.trim)
      val randomWords = Random.shuffle(potentialWords.filter { potentialWord =>
        potentialWord.takeRight(3) == word.takeRight(3)
      })

      randomWords.map(wordAnalyser.analyseWord(_)).filter(_.isDefined).map(_.get).filter { info =>
        info.cardinality == result.cardinality && info.gender == result.gender
      }.headOption.map(_.word)
    }.flatten

  }
}
